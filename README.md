# Auto-host ![image of a Toast](./doc/atoast_small.png "A Toast")    
  
## Description  
  
Auto-host provides a quick, easy and secure way to host web apps by yourself.  
All you're data is host on you're computer and your apps is protected by Tor.  
It provides to you an `.onion` adress (an adress only accessible through Tor), so you're user are also protected.  
No logs are saved to protect users.  
It's based on docker scripts to create "virtual machines" (containers).  
It can be also used to discover open-source web apps.  
  
## Table of Contents
- [The Apps](#the-apps)
    - [Publishing website / blogs](#publishing-website-blogs)
        - [IndyMedia WordPress](#indymedia-wordpress)
        - [SPIP](#spip)
        - [WordPress](#wordpress)
- [Install](#install)
    - [Requirements](#requirements)
    - [IndyMedia WordPress install](#indymedia-wordpress-install)
    - [SPIP install](#spip-install)
    - [WordPress install](#wordpress-install)
- [How to use](#how-to-use)
- [Architecture Quick Review](#architecture-quick-review)
- [DEV Section](#dev-section)

##  The Apps  
		
### Publishing website / blogs  

- **IndyMedia WordPress**  
		Adapting WordPress to run an Indymedia website.
		Indymedia websites are using open-publishing so that contributors can openly and easily share news from the streets without the filtering and tracking faced when using media corporations or social medias.  
		Indymedia websites usually handle publications like articles, events and short stories.  
		The goal of this project is to adapt WordPress to run an IMC.  
		https://indymedia.org  
		https://en.wikipedia.org/wiki/Indymedia  
		DEV : http://wmj5kiic7b6kjplpbvwadnht2nh2qnkbnqtcv3dyvpqtz7ssbssftxid.onion/imcpress/imcpress  
		or https://0xacab.org/imcpress/imcpress  
- **SPIP**  
		SPIP is a publishing system for the Internet in which great importance is attached to collaborative working, to multilingual environments, and to simplicity of use for web authors. It is free software, distributed under the GNU/GPL licence. This means that it can be used for any Internet site, whether personal or institutional, non-profit or commercial.  
		https://www.spip.net  
- **WordPress**  
		WordPress is a web content management system. It was originally created as a tool to publish blogs but has evolved to support publishing other web content, including more traditional websites, mailing lists and Internet forum, media galleries, membership sites, learning management systems and online stores.  
		https://wordpress.com/  
  

## Install  
### Requirements   
  
 - Docker ( version > 20.10 )  
 [https://docs.docker.com/engine/install/](https://docs.docker.com/engine/install/)  
 - Docker Compose ( version > 1.29 )  
 [https://docs.docker.com/compose/install/](https://docs.docker.com/compose/install/) 
 
 
OpenSSL is not required but recommanded (used for password generation).  
Tested with x86_64 Debian 10  (it will work with Ubuntu).  

#### Windows  
It may work with Docker for Windows with "git for windows" (but not fully tested).  
 https://gitforwindows.org  
It provide Git bash environment that is used by install/config scripts.
  
### IndyMedia WordPress install  

Clone this repos:  
`git clone git@wmj5kiic7b6kjplpbvwadnht2nh2qnkbnqtcv3dyvpqtz7ssbssftxid.onion:alt.n4/lamp.git`  
or  
`git clone git@0xacab.org:alt.n4/lamp.git`  
  
then install:  
`./scripts/install_imcpress.sh`  

It will build docker images, then download / install WordPress and IndyMedia templates/plugins.  
When finised it will give you the onion adress to use.  
Finish step are necessary through browser : you must configure an admin wordpress user.  
After this step you must activate "IMCPress" in "Appearance" and "Plugin" sections.
You can also use fakerpress plugin to quickly generate articles and see results.

  
### SPIP install   

Clone this repos:  
`git clone git@wmj5kiic7b6kjplpbvwadnht2nh2qnkbnqtcv3dyvpqtz7ssbssftxid.onion:alt.n4/lamp.git`  
or  
`git clone git@0xacab.org:alt.n4/lamp.git`  
  
then install:  
`./scripts/install_spip.sh`  

Finish step are necessary through browser.  
/!\ Warning : Database adress must be `127.0.0.1` not `localhost`  
  
### WordPress install  

Clone this repos:  
`git clone git@wmj5kiic7b6kjplpbvwadnht2nh2qnkbnqtcv3dyvpqtz7ssbssftxid.onion:alt.n4/lamp.git`  
or  
`git clone git@0xacab.org:alt.n4/lamp.git`  
  
then install:  
`./scripts/install_wordpress.sh`  

It will build docker images, then download / install WordPress and plugins.  
When finised it will give you the onion adress to use.  
Finish step are necessary through browser : you must configure an admin wordpress user.  
You can also use fakerpress plugin to quickly generate articles and see results.


## How to use  
  
- `start.sh`  
  Launch web server and show config.  
- `stop.sh`  
  Stop the server  
- `reset.sh`  
	Will quickly uninstall and reinstall the server, all of your datas will be lost.  
- `uninstall.sh`  
	Uninstall the app, delete datas, you can install a new app now  
- `passwords.txt`  
	Will list all of your automatic password generateds that you may know. (you can delete informations in there if you want but the file must exists).  
- `./scripts/reset_tor_hidden_service.sh`  
	Will reset your onion adress.  
  
## Architecture Quick Review  
  
![A Toast Architecture](./doc/atoast_archi.png "A Toast Architecture")  
  
Your webapp is installed in a sort of "virtual machine" (in Docker containers).  
If your web application must download something, it can though the "tor-router" but all other trafic is blocked by the router.  
It's not easy for beginners to open a web server to the world, but with hidden services, Tor will do all steps for you.  
It will publish your .onion adress to some "rendez-vous point" so, users can contact your server through them.  
User is also protected with the no-log policy by default of this webserver.    
  
![Tor logo](./doc/Tor-logo-2011-flat_small.png "Tor logo") https://www.torproject.org/  /  https://en.wikipedia.org/wiki/Tor_(network)  
  

## DEV Section  

[README_DEV.md](README_DEV.md)  
  