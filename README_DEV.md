# Auto-host ![image of a Toast](./doc/atoast_small.png "A Toast")    
  
## The DEV Apps  
  
- **PhpMyAdmin** (for Dev)  
	phpMyAdmin is a free software tool written in PHP, intended to handle the administration of MySQL over the Web. phpMyAdmin supports a wide range of operations on MySQL and MariaDB. Frequently used operations (managing databases, tables, columns, relations, indexes, users, permissions, etc) can be performed via the user interface, while you still have the ability to directly execute any SQL statement.  
	https://www.phpmyadmin.net/  
- **Empty Lamp Server** (for Dev)  
		Empty server, you can add php app in `./data/html` directory
		A database user and pass is already configured for quick install. (but you can change it or change install scripts)  
		You can test your server through onion adress provided.  
		**/!\ Warning**: no logs are saved, so you may have to activate them if you want to debug app/scripts  
  

## Directories  
  
- `./scripts/install_XXXX.sh` (install the XXX App you want to use)  
- your www files are here : `./data/html` (put html files or php script here)  
- apache config files are here : `./data/etc_apache2_config`  
- mariaDB files are here : `./data/mariadb_data`  
- mariaDB config files are here : `./data/mariadb_config`  
- php config files are here : `./data/php_config`  
- tor config files are here : `./data/tor` ( hidden service is here : `./data/tor/hidden_service` ) 
  
  
## Install  
  
Read / Modify all the scripts for your usages !  
   
### PhpMyAdmin  

Clone this repos:  
`git clone git@wmj5kiic7b6kjplpbvwadnht2nh2qnkbnqtcv3dyvpqtz7ssbssftxid.onion:alt.n4/lamp.git`  
or  
`git clone git@0xacab.org:alt.n4/lamp.git`  
  
then install:  
`./scripts/install_phpmyadmin.sh`  

It will build docker images, then download / install PhpMyAdmin.  
  
### Empty Lamp Server  

Clone this repos:  
`git clone git@wmj5kiic7b6kjplpbvwadnht2nh2qnkbnqtcv3dyvpqtz7ssbssftxid.onion:alt.n4/lamp.git`  
or  
`git clone git@0xacab.org:alt.n4/lamp.git`  
  
then install IMPress:  
`./scripts/install_emptylamp.sh`  

It will build docker images, then start your web server.  
Is an empty lamp server with a DB user configured.  
All generated passwords are stored here : `./passwords.txt`  
You can use `./scripts/show_config.sh` to show your config.  
  
## Update applications 
  
Stop the services:  
`./stop.sh`  

backup directories:  
- `./data/html`  
- `./data/mariadb_data`  
- `./passwords.txt`  
- `./start.sh`  
- `./stop.sh`  
- `./reset.sh`  
- `./uninstall.sh`  

make a new clone of this git repos  
`git clone git@wmj5kiic7b6kjplpbvwadnht2nh2qnkbnqtcv3dyvpqtz7ssbssftxid.onion:alt.n4/lamp.git`  
or  
`git clone git@0xacab.org:alt.n4/lamp.git`  

restore files  

rebuild docker images :
`./scripts/rebuild_docker_images.sh`  

and restart server : `./start.sh`

  
## Architecture  
  
![A Toast Architecture](./doc/atoast_archi.png "A Toast Architecture")  
  
### tor-router
is a gateway (as whonix gateway), all internet trafic is configure to use Tor, it give internet access to other container.  
It's configured to have a hidden service installed (on port 80)  
If you have Tor blocked in your country, you can change `torcc` file in `./data/tor/torcc` and add restrictions / bridges [https://tb-manual.torproject.org/bridges/](https://tb-manual.torproject.org/bridges/)  
Many server apps use internet access (automatic updates of WordPress, other services as GeoIP, S3 etc...) so informations may leaks and be seen by your ISP.  
With a Tor gateway, all your installed apps are Tor protected.
  
### apache
is a standard apache webserver, it use port 80 to be the tor hidden service webserver

### mariadb
is a standard mariadb database server

### php-fpm
is a standard php-fpm server, it is configured in apache to be used for php scripts
  
## Recommandations  
  
You can start with having a threat model, so you will list the needs you have. (Journalist, Activist, etc...).  
Needs differs with different context and menaces you have.  
So you may need data encryption, data wipe solution.  
  
**SECTION IS IN PROGRESS**  

- Backup / Restore  
	You have to backup you're files (can be all files of the directory) in a secure location.  
	In case of host crash (hardware / software), you may have a way to create in an other physical machine the host environment (with docker).  
	Depending of you're needs, you may keep a backup on a different adress, so it may be resistant to a disaster.  
	https://en.wikipedia.org/wiki/Backup  
- Threat model  
	https://en.wikipedia.org/wiki/Threat_model
- Data encryptions  
	You may encrypt your files, so it's recommanded to install all of this apps in a veracrypt volume.
	You may use hidden volume.  
	![image of Hidden volume](./doc/hidden_volume.gif "Hidden volume")  
	https://en.wikipedia.org/wiki/Deniable_encryption  
	https://en.wikipedia.org/wiki/Disk_encryption_software  
- Data wipe  
	https://en.wikipedia.org/wiki/Data_erasure  


