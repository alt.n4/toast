#!/bin/bash

set -e

cd $(git rev-parse --show-toplevel)

source ./tor-router/functions.sh
source ./scripts/functions.sh

if test -f "passwords.txt"; then
	echo ""
	echo ""
	echo ""
	echo "A website is already installed !"
	echo "Please use uninstall first !"
    ./scripts/show_config.sh
	exit
fi

test_tor_proxy

build_image apache:1.0 ./image/apache
build_image mariadb:1.0 ./image/mariadb
build_image php-fpm:1.0 ./image/php-fpm

docker-compose stop

./scripts/reset_apache.sh
./scripts/reset_mariadb.sh
./scripts/uninstall_lamp.sh

docker-compose up -d tor-router-debian
docker-compose up -d apache
docker-compose up -d mariadb
docker-compose up -d php-fpm

generate_password_09AZaz
DBPass=$password
generate_password_09AZaz
DBPass=$DBPass$password

wait_mariaDB_starting

# Configure database with user/password access
docker exec -it lamp_mariadb_1 bash -c "mysql -e 'CREATE DATABASE IF NOT EXISTS datab;'"
docker exec -it lamp_mariadb_1 bash -c "mysql -e 'CREATE USER IF NOT EXISTS \"user\"@\"127.0.0.1\" IDENTIFIED BY \"$DBPass\";'"
docker exec -it lamp_mariadb_1 bash -c "mysql -e 'GRANT ALL PRIVILEGES ON datab.* TO \"user\"@\"127.0.0.1\"'";
docker exec -it lamp_mariadb_1 bash -c "mysql -e 'FLUSH PRIVILEGES;'"


echo "" > passwords.txt
echo "" >> passwords.txt
echo "Empty Lamp server installed !" >> passwords.txt
echo "/!\ Database address is not localhost !" >> passwords.txt
echo "Database address : 127.0.0.1" >> passwords.txt
echo "Database name : datab" >> passwords.txt
echo "Database user : user" >> passwords.txt
echo "Database password : $DBPass" >> passwords.txt
# Config install 
cp ./scripts/start_lamp.sh ./start.sh
cp ./scripts/stop_lamp.sh ./stop.sh
echo "./scripts/uninstall_lamp.sh" > ./reset.sh
echo "./scripts/install_emptylamp.sh" >> ./reset.sh
chmod +x ./reset.sh
cp ./scripts/uninstall_lamp.sh ./uninstall.sh

./scripts/show_config.sh
