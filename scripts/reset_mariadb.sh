#!/bin/bash

set -e

cd $(git rev-parse --show-toplevel)

source ./tor-router/functions.sh
source ./scripts/functions.sh

docker-compose stop mariadb

rm -Rf ./data/mariadb_data && mkdir ./data/mariadb_data
