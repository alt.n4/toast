#!/bin/bash

set -e

cd $(git rev-parse --show-toplevel)

source ./tor-router/functions.sh
source ./scripts/functions.sh


docker-compose stop apache
docker-compose stop php-fpm

rm -Rf ./data/html && mkdir ./data/html
