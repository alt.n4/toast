#!/bin/bash

set -e

cd $(git rev-parse --show-toplevel)

source ./tor-router/functions.sh
source ./scripts/functions.sh

if test -f "passwords.txt"; then
	echo ""
	echo ""
	echo ""
	echo "A website is already installed !"
	echo "Please use uninstall first !"
    ./scripts/show_config.sh
	exit
fi


test_tor_proxy

build_image apache:1.0 ./image/apache
build_image mariadb:1.0 ./image/mariadb
build_image php-fpm:1.0 ./image/php-fpm

docker-compose stop

./scripts/reset_apache.sh
./scripts/reset_mariadb.sh
./scripts/uninstall_lamp.sh

docker-compose up -d tor-router-debian
docker-compose up -d apache
docker-compose up -d mariadb
docker-compose up -d php-fpm

echo "Download wordpress"
docker run -it --rm -v $(pwd)/data/html:/root/test --network=container:toast_tor-router-debian_1 php-fpm:1.0 sh -c "cd /root/test && git clone --depth 1 https://github.com/WordPress/WordPress.git && mv ./WordPress/* .;rm -Rf ./WordPress"
echo "Download plugins"
docker run -it --rm -v $(pwd)/data/html:/root/test -v $(pwd)/data/imcpress:/root/imcpress --network=container:toast_tor-router-debian_1 php-fpm:1.0 sh -c "cd /root/test/wp-content/plugins && curl -L -o fakerpress.0.5.3.zip https://downloads.wordpress.org/plugin/fakerpress.0.5.3.zip && apt install unzip && unzip fakerpress.0.5.3.zip && rm fakerpress.0.5.3.zip"
docker run -it --rm -v $(pwd)/data/html:/root/test -v $(pwd)/data/imcpress:/root/imcpress --network=container:toast_tor-router-debian_1 php-fpm:1.0 sh -c "cd /root/test/wp-content/plugins && curl -L -o unbloater.1.5.5.zip https://downloads.wordpress.org/plugin/unbloater.1.5.5.zip && apt install unzip && unzip unbloater.1.5.5.zip && rm unbloater.1.5.5.zip"
chmod 777 ./data/html/wp-content/plugins/

generate_password_09AZaz
DBPass=$password
generate_password_09AZaz
DBPass=$DBPass$password

# Config php files
## Copy preconfigured wp-config (for testing)
cp ./data/website_config/wordpress/wp-config.php ./data/html
sed -i "s/\*\*DBpassword\*\*/$DBPass/g" ./data/html/wp-config.php
generate_password_09AZaz ; pass=$password ; generate_password_09AZaz ; pass=$pass$password
sed -i "s/\*\*AUTH_KEY\*\*/$pass/g" ./data/html/wp-config.php
generate_password_09AZaz ; pass=$password ; generate_password_09AZaz ; pass=$pass$password
sed -i "s/\*\*SECURE_AUTH_KEY\*\*/$pass/g" ./data/html/wp-config.php
generate_password_09AZaz ; pass=$password ; generate_password_09AZaz ; pass=$pass$password
sed -i "s/\*\*LOGGED_IN_KEY\*\*/$pass/g" ./data/html/wp-config.php
generate_password_09AZaz ; pass=$password ; generate_password_09AZaz ; pass=$pass$password
sed -i "s/\*\*NONCE_KEY\*\*/$pass/g" ./data/html/wp-config.php
generate_password_09AZaz ; pass=$password ; generate_password_09AZaz ; pass=$pass$password
sed -i "s/\*\*AUTH_SALT\*\*/$pass/g" ./data/html/wp-config.php
generate_password_09AZaz ; pass=$password ; generate_password_09AZaz ; pass=$pass$password
sed -i "s/\*\*SECURE_AUTH_SALT\*\*/$pass/g" ./data/html/wp-config.php
generate_password_09AZaz ; pass=$password ; generate_password_09AZaz ; pass=$pass$password
sed -i "s/\*\*LOGGED_IN_SALT\*\*/$pass/g" ./data/html/wp-config.php
generate_password_09AZaz ; pass=$password ; generate_password_09AZaz ; pass=$pass$password
sed -i "s/\*\*NONCE_SALT\*\*/$pass/g" ./data/html/wp-config.php

wait_mariaDB_starting

# Configure database with user/password access
docker exec -it lamp_mariadb_1 bash -c "mysql -e 'CREATE DATABASE IF NOT EXISTS wordpress;'"
docker exec -it lamp_mariadb_1 bash -c "mysql -e 'CREATE USER IF NOT EXISTS \"wpuser\"@\"127.0.0.1\" IDENTIFIED BY \"$DBPass\";'"
docker exec -it lamp_mariadb_1 bash -c "mysql -e 'GRANT ALL PRIVILEGES ON wordpress.* TO \"wpuser\"@\"127.0.0.1\"'";
docker exec -it lamp_mariadb_1 bash -c "mysql -e 'FLUSH PRIVILEGES;'"

echo "" > passwords.txt
echo "" >> passwords.txt
echo "WordPress installed !" >> passwords.txt
echo "/!\ Database address is not localhost !" >> passwords.txt
echo "Database address : 127.0.0.1" >> passwords.txt
echo "Database name : wordpress" >> passwords.txt
echo "Database user : wpuser" >> passwords.txt
echo "Database password : $DBPass" >> passwords.txt
# Config install 
cp ./scripts/start_lamp.sh ./start.sh
cp ./scripts/stop_lamp.sh ./stop.sh
echo "./scripts/uninstall_lamp.sh" > ./reset.sh
echo "./scripts/install_wordpress.sh" >> ./reset.sh
cp ./scripts/uninstall_lamp.sh ./uninstall.sh

./scripts/show_config.sh
