function generate_password() {

# generate_password
# echo Pass : $password

	local pass=""
	local pass1=""
	
	pass=$(openssl rand 2048 | tr -dc \@\(\)\*\=\+_A-Z-a-z-0-9 | head -c${1:-32};echo;)
	pass1=$(openssl rand 2048 | tr -dc \@\(\)\*\=\+_A-Z-a-z-0-9 | head -c${1:-32};echo;)
	if [ "$pass" != "$pass1" ] && [ "${#pass}" == "32" ]; then
		password=$pass
		return
	fi
	
	pass=$(< /dev/urandom tr -dc \@\(\)\*\=\+_A-Z-a-z-0-9 | head -c${1:-32};echo;)
	pass1=$(< /dev/urandom tr -dc \@\(\)\*\=\+_A-Z-a-z-0-9 | head -c${1:-32};echo;)
	if [ "$pass" != "$pass1" ] && [ "${#pass}" == "32" ]; then
		password=$pass
		return
	fi
	generate_password_09AZaz
}

function generate_password_09AZaz() {

# generate_password_09AZaz
# echo Pass : $password

	local pass=""
	local pass1=""
	
	pass=$(openssl rand -base64 256 | sha256sum | base64 | head -c 32 ; echo)
	pass1=$(openssl rand -base64 256 | sha256sum | base64 | head -c 32 ; echo)
	if [ "$pass" != "$pass1" ] && [ "${#pass}" == "32" ]; then
		password=$pass
		return
	fi
	
	pass=$(< /dev/urandom tr -dc A-Z-a-z-0-9 | head -c${1:-32};echo;)
	pass1=$(< /dev/urandom tr -dc A-Z-a-z-0-9 | head -c${1:-32};echo;)
	if [ "$pass" != "$pass1" ] && [ "${#pass}" == "32" ]; then
		password=$pass
		return
	fi
	
	pass=$(date +%sN | sha256sum | base64 | head -c 32 ;sleep 1; echo)
	pass1=$(date +%sN | sha256sum | base64 | head -c 32 ;sleep 1; echo)
	if [ "$pass" != "$pass1" ] && [ "${#pass}" == "32" ]; then
		password=$pass
		return
	fi
	return 1
}

function wait_mariaDB_starting() {
	for i in $(seq 1 60);
	do
		mdbv=$(docker exec -it lamp_mariadb_1 bash -c "mysql -e 'SELECT version();'" | grep "MariaDB") || mdbv="";
		if [ "${#mdbv}" != "0" ]; then
			echo "MariaDB is ready"
			break 1
		fi
		sleep 1
	done
}
