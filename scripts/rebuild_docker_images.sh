#!/bin/bash

set -e

cd $(git rev-parse --show-toplevel)

source ./tor-router/functions.sh
source ./scripts/functions.sh

docker-compose stop

docker_remove_image apache:1.0 || true
docker_remove_image mariadb:1.0 || true
docker_remove_image php-fpm:1.0 || true

build_image_nocache apache:1.0 ./image/apache
build_image_nocache mariadb:1.0 ./image/mariadb
build_image_nocache php-fpm:1.0 ./image/php-fpm
