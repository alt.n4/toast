#!/bin/bash

set -e

cd $(git rev-parse --show-toplevel)

source ./tor-router/functions.sh
source ./scripts/functions.sh

docker-compose stop

./scripts/reset_apache.sh
./scripts/reset_mariadb.sh
./scripts/reset_tor_hidden_service.sh

if test -f "passwords.txt"; then
    rm passwords.txt
	rm start.sh
	rm stop.sh
	rm reset.sh
	rm uninstall.sh
fi
