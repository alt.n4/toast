#!/bin/bash

set -e

cd $(git rev-parse --show-toplevel)

source ./tor-router/functions.sh
source ./scripts/functions.sh

onionname=$(cat ./data/tor/hidden_service/hostname)

echo "Tor Hidden Service is started here :"
echo "http://${onionname}/"

if test -f "passwords.txt"; then
    cat passwords.txt
fi
