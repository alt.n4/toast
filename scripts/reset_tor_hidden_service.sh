#!/bin/bash

set -e

cd $(git rev-parse --show-toplevel)

source ./tor-router/functions.sh
source ./scripts/functions.sh

# Reset TOR Hidden Service
rm -Rf ./data/tor/hidden_service
