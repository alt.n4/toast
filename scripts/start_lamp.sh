#!/bin/bash

set -e

cd $(git rev-parse --show-toplevel)

source ./tor-router/functions.sh
source ./scripts/functions.sh

docker-compose up -d apache
docker-compose up -d mariadb
docker-compose up -d php-fpm

wait_mariaDB_starting

./scripts/show_config.sh