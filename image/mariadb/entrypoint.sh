#!/bin/bash

# If command is provided run that
if [ "${1:0:1}" != '-' ]; then
  exec "$@"
fi

chown -R mysql:mysql /var/lib/mysql

if [ ! -f /var/lib/mysql/ibdata1 ]; then
    echo "Databases not found! Create them !"
	mariadb-install-db
fi

mysqld