#!/bin/bash

# If command is provided run that
if [ "${1:0:1}" != '-' ]; then
  exec "$@"
fi