<?php

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '**AUTH_KEY**' );
define( 'SECURE_AUTH_KEY',  '**SECURE_AUTH_KEY**' );
define( 'LOGGED_IN_KEY',    '**LOGGED_IN_KEY**' );
define( 'NONCE_KEY',        '**NONCE_KEY**' );
define( 'AUTH_SALT',        '**AUTH_SALT**' );
define( 'SECURE_AUTH_SALT', '**SECURE_AUTH_SALT**' );
define( 'LOGGED_IN_SALT',   '**LOGGED_IN_SALT**' );
define( 'NONCE_SALT',       '**NONCE_SALT**' );

/**#@-*/
