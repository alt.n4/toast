#!/bin/bash

# Configure environment variables
# TOR_ROUTER_USER="${TOR_ROUTER_USER:-tor-router}"
# TOR_ROUTER_UID="${TOR_ROUTER_UID:-9001}"
# TOR_ROUTER_HOME="${TOR_ROUTER_HOME:-/opt/tor-router}"
# TOR_CONFIG_FILE="${TOR_CONFIG_FILE:-${TOR_ROUTER_HOME}/torrc}"

# echo 'Setting up container'

# Setup the TOR_ROUTER_USER
# chown -R ${TOR_ROUTER_UID}:${TOR_ROUTER_UID} ${TOR_ROUTER_HOME}
# adduser --quiet --disabled-password --shell /bin/bash --home "${TOR_ROUTER_HOME}" --uid "${TOR_ROUTER_UID}" --gecos "${TOR_ROUTER_USER}" "${TOR_ROUTER_USER}"


# Run tor as the TOR_ROUTER_USER
# echo 'Starting the Tor router'
# exec sudo -u "${TOR_ROUTER_USER}" tor -f "${TOR_CONFIG_FILE}" &


# Configure environment variables
TOR_ROUTER_USER="${TOR_ROUTER_USER:-tor-router}"
TOR_ROUTER_UID="${TOR_ROUTER_UID:-9001}"
TOR_ROUTER_HOME="${TOR_ROUTER_HOME:-/opt/tor-router}"
TOR_CONFIG_FILE="${TOR_CONFIG_FILE:-${TOR_ROUTER_HOME}/torrc}"

echo 'Setting up container'

# Setup the TOR_ROUTER_USER
chown -R ${TOR_ROUTER_UID}:${TOR_ROUTER_UID} ${TOR_ROUTER_HOME}
adduser --quiet --disabled-password --shell /bin/bash --home "${TOR_ROUTER_HOME}" --uid "${TOR_ROUTER_UID}" --gecos "${TOR_ROUTER_USER}" "${TOR_ROUTER_USER}"


echo 'Starting Apache'
# /usr/sbin/apache2ctl -D FOREGROUND
/usr/sbin/apache2ctl start

# Run tor as the TOR_ROUTER_USER
echo 'Starting the Tor router'
exec sudo -u "${TOR_ROUTER_USER}" tor -f "${TOR_CONFIG_FILE}"

